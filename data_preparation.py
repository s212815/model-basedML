# -*- coding: utf-8 -*-
"""
Original file is located at
    https://colab.research.google.com/drive/1J1UdFWjoYNI5b1WbmqPC9Nj2KV_FTDGD

Students:
*   Alexandre Bernard--Michinov, s212815
*   Henan Zhao, s202848
*   Mehmet Eyyupoglu, s174448
"""


#################################################################################################################################
### IMPORTS
#################################################################################################################################
import numpy as np
import pandas as pd
from datetime import datetime

#################################################################################################################################
### INPUTS
#################################################################################################################################
team = '1625'
league = 'England'
target = '#cumulative_points'

#################################################################################################################################
### LOADING RAW DATA
#################################################################################################################################
"""Importing team data and filtering on the correct league"""
teams = pd.read_json('https://gitlab.gbar.dtu.dk/s212815/model-basedML/raw/master/data/raw_data/teams.json')
teams['league'] = [item['name'] for item in teams['area']]
teams = teams[teams['league'] == league]
teams = teams[teams['type'] == 'club']

"""Importing players data"""
players = pd.read_json("https://gitlab.gbar.dtu.dk/s212815/model-basedML/raw/master/data/raw_data/players.json")
players = players[players['currentTeamId'].isin(teams['wyId'])]
players['birthDate'] = pd.to_datetime(players['birthDate'])
players['age'] = round((datetime(2019,5,1) - players['birthDate']).dt.days/365.25)
players = players[players['currentTeamId'] == int(team)]

"""Importing games"""
matches = pd.read_json(f"https://gitlab.gbar.dtu.dk/s212815/model-basedML/raw/master/data/raw_data/matches_{league}.json")
matches = matches.drop(['venue', 'date', 'referees', 'competitionId', 'seasonId', 'duration'], axis=1)
matches['team1'] = [list(item.keys())[0] for item in matches['teamsData']]
matches['team2'] = [list(item.keys())[1] for item in matches['teamsData']]

"""For one team, extract all its game on the season and compute metrics"""
team_season = pd.concat([matches[matches['team1'] == team],  matches[matches['team2'] == team]]).sort_values(by='gameweek')
team_season['opposite_team'] = [team_season['team1'][i] if team_season['team1'][i] != team else team_season['team2'][i] for i in team_season.index]
team_season['#goals'] = [item[team]['score'] for item in team_season['teamsData']]
team_season['#cumulative_goals'] = np.cumsum(np.array(team_season['#goals']))
team_season['#conceded_goals'] = [team_season['teamsData'][i][team_season['opposite_team'][i]]['score'] for i in team_season.index]
team_season['#cummulative_conceded_goals'] = np.cumsum(np.array(team_season['#conceded_goals']))
team_season['#points'] = (team_season['winner'] == int(team)).astype(int) * 3 + (team_season['winner'] == 0).astype(int)
team_season['#cumulative_points'] = np.cumsum(np.array(team_season['#points']))

"""Importing player scores"""
playerank = pd.read_json("https://gitlab.gbar.dtu.dk/s212815/model-basedML/raw/master/data/raw_data/playerank.json")
playerank = playerank[playerank['matchId'].isin(set(team_season['wyId']))]
playerank = playerank.merge(team_season[['wyId', 'gameweek']], left_on='matchId', right_on='wyId').drop('wyId', axis=1)

"""Getting data ready"""
pivot_playerank = playerank.pivot(index='playerId', columns='gameweek', values='playerankScore')
## Cumulative average score of the player
## Improvement TODO: take moving average instead of mean
X_team = (pivot_playerank.shift(periods=1, axis=1).cumsum(axis=1)/(pivot_playerank.columns-1)).fillna(0)
X_inputs = X_team[X_team.index.isin(set(players['wyId']))]

#################################################################################################################################
### EXPORTS
#################################################################################################################################
team_season[target].to_csv('data/model_data/outcome.csv')
X_inputs.to_csv('data/model_data/inputs.csv')



