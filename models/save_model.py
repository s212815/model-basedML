## Allow to save model in txt file

model_definition = """
data{
int<lower=0> T;
int<lower=0> T_forecast;
vector[T] y;
int<lower=0> P;
matrix[P,T] X;
}
parameters{
vector[2] beta;
vector[P] W;
real<lower=0> tau;
real<lower=0> sigma;
vector[T] z;
}
model{ 
beta ~ cauchy(0,10);
tau ~ normal(0,10);
sigma ~ normal(0,10);
W ~ normal(0,10);

z[1] ~ normal(transpose(W) * X[:,1], tau);
z[2] ~ normal(beta[1] * z[1] + transpose(W) * X[:,2], tau);
for (t in 1:T-2){
    z[t+2] ~ normal(beta[1] * z[t] + beta[2] * z[t+1] + transpose(W) * X[:,t], tau);
}
// y[t] ~ categorical(softmax(z));
y ~ normal(z,tau);
}
generated quantities{
vector[T_forecast] y_hat;
vector[T_forecast] z_hat;

z_hat[1] = normal_rng(beta[1] * z[T-2] + beta[2] * z[T-1], tau);
z_hat[2] = normal_rng(beta[1] * z[T-1] + beta[2] * z_hat[1] , tau);
for (t in 1:T_forecast-2){
    z_hat[t+2] = normal_rng(beta[1] * z_hat[t] + beta[2] * z_hat[t+1], tau);
}

for (t in 1:T_forecast)
    y_hat[t] = normal_rng(z_hat[t], sigma);
}
"""  

with open('temporal_model.txt', 'w') as f:
    f.write(model_definition)